Some distributions have a gimped ffmpeg.....


Ubuntu Users: https://wiki.ubuntu.com/ffmpeg

  sudo apt-get build-dep ffmpeg;
  sudo apt-get install liblame-dev libfaad2-dev libfaac-dev libxvidcore4-dev liba52-0.7.4 liba52-0.7.4-dev libdts-dev
  apt-get source ffmpeg
  cd ffmpeg-*/

  In pre-7.10 releases (these should be all on one line):

    ./configure --enable-gpl --enable-pp --enable-vorbis --enable-libogg --enable-a52 \
      --enable-dts --enable-dc1394 --enable-libgsm --disable-debug --enable-mp3lame \
      --enable-faad --enable-faac --enable-xvid --enable-shared 

  Or, on 7.10:

    ./configure --enable-gpl --enable-pp --enable-libvorbis --enable-libogg \
      --enable-liba52 --enable-libdts --enable-dc1394 --enable-libgsm --disable-debug \
      --enable-libmp3lame --enable-libfaad --enable-libfaac --enable-xvid --enable-shared 

  make
  sudo checkinstall -D make install




MacUsers

Install MacPorts (use google for help) 
sudo port install ffmpeg +gpl +lame +x264 +xvid +extvorbis +theora +faac +faad +xvid +x264 +a52


